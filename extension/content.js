
chrome.runtime.onMessage.addListener(function(message, sender, reply) {
	
	var addHoursLink = document.getElementById('issue-view-tempo-panel-log-work');

	if(message === 'jira-nag_addNag') {
		
		var nagDiv = document.createElement('div');
		nagDiv.classList.add("jira-nag-nag-div", "jira-nag-corner-0");

		var linkEl = document.createElement('a');
		linkEl.href = '#';
		linkEl.addEventListener('click', function() {
			addHoursLink.click();
		});
		linkEl.appendChild(document.createTextNode('Log time?'));

		nagDiv.appendChild(linkEl);

		document.body.appendChild(nagDiv);


		nagDiv.addEventListener("mouseover", function() {
			nagDiv._jiraNag_over = true;
		});
		nagDiv.addEventListener("mouseleave", function() {
			nagDiv._jiraNag_over = false;
		});


		setInterval(function() {
			var classList = nagDiv.classList,
				i, clazz, cornerNumber;

			if(nagDiv._jiraNag_over) {
				return;
			}

			for(i = 0; i < classList.length; i++) {
				clazz = classList.item(i);
				if(clazz.search("jira-nag-corner-") != -1) {
					cornerNumber = Number(clazz.slice(clazz.lastIndexOf("-") + 1));
					classList.remove(clazz);
					classList.add("jira-nag-corner-" + ((cornerNumber + 1) % 4));
					break;
				}
			}
		}, 3000);
	}
	else if(message === 'jira-nag_isWorkLogEnabled') {
		reply(!!addHoursLink);
	}
});