
chrome.webNavigation.onCompleted.addListener(function(event) {

	chrome.tabs.sendMessage(event.tabId, 'jira-nag_isWorkLogEnabled', function(answer) {
		if(answer === true) {
			chrome.pageAction.show(event.tabId);

			//mark the page for nagging
			chrome.tabs.sendMessage(event.tabId, 'jira-nag_addNag');
		}
	});

});